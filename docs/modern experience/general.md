![08.configurepanel](../images/modern/08.configurepanel.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Create new BT Alert List](./listsettings/#create-a-list)
- [List Settings](./listsettings#list-url)
- [Web Part Appearance](./appearance.md)
- [Advanced Options](./advanced.md)
- [Performance](./performance.md)
- [Web Part Messages](./message.md)