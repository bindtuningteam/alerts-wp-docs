### Create a list

This list is what you will use to store your alerts. Let's create your list:

1. Place your modern page in Edit mode;

    ![edit-page](../images/modern/01.edit-page-modern.png)

2. Mouse hover the web part and click on the pencil (✏️) icon that will appear or click on **Configure**;

    ![configure](../images/modern/02.configure.png)

3. Type the name of the list and hit **Create the List**;

    ![list url](../images/modern/04.createlist.png)     
    
4. The list will be created on your current Site and connected with the Web Part;

### Add List URL

If you have already configured a BindTuning alerts list, you need to paste the **list URL** that you have copied when you finished to create the list.

![list url](../images/modern/03.listurl.png)

<p class="alert alert-info">We recommend you use relative paths in this field. So instead of using a URL like  https://company.sharepoint.com/sites/Home/Lists/Alerts you should use something like <b>/sites/Home/Lists/Alerts</b>. This will ensure that the web part will work regardless of how you’re accessing the site. </p>