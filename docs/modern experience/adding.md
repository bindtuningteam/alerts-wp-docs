1. Open the page where you want to add the web part and click on the **Edit** button;
    
    ![edit-page](../images/modern/01.edit-page-modern.png)
    
2. Click on the **[+]** button to add a new web part or section to your page;

    ![add-section](../images/modern/07.addpart.png)


3. On the web part panel, search for **BindTuning** to filter the web parts. Click on the **Alerts Web Part**;
    
	![modern-part](../images/modern/06.modernpart.png)

Now the only thing left to do is to connect the web part and setup the rest of the **[Web Part Properties](./general.md)**.