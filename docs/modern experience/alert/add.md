1. Click on the **Edit** button;

	![edit-page](../../images/modern/01.edit-page-modern.png)

2. Mouse hover the Web Part and click on the **[+]** button to add a new Alert;

	![Add-Alert](../../images/modern/11.add_alert.png)

3. Fill out the form that pops up. You can check out what you need to do in each setting in the <a href="../../global/alertsettings">Alerts Settings</a> section of this User Guide;

4. After setting everything up, click on the **Preview** button if you want to see how everything looks on the page, or click on **Save** or **Save and create another**.

	![buttons_newalert.png](https://bitbucket.org/repo/ekxgn66/images/2357822962-buttons_newalert.png)

<p class="alert alert-success">Clicking <b>Save and create another</b> will keep the form open, so you can add more alerts to your page without closing the form.</p>