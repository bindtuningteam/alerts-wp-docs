1. Click on the **Edit** button;

2. On the web part, click on the **Manage Alerts** icon;

	![Manage](../../images/modern/12.manage_alert.png)

3. The list of alerts will appear. Click the **trash can** icon to delete the alert that you want to remove;

	![Delete](../../images/modern/13.delete_alert.png)

4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the alert will be removed.	