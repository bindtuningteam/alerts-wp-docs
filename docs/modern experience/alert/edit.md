1. Click on the **Edit** button;

2. On the web part sidebar click the **Manage Alerts** icon;

	![Manage](../../images/modern/adding/12.manage_alert.png)

3. The list of alerts will appear. Click the **pencil icon** to edit an alert;

	![Edit](../../images/modern/adding/14.edit_alert.png)

4. Change the <a href="../../global/alertsettings">Alerts Settings</a> as necessary to re-configure the alert.

	![editalertmodal.png](https://bitbucket.org/repo/ekxgn66/images/1694895613-editalertmodal.png)

5. Done editing? Click on the **Preview** button if you want to see how everything looks on the page or on **Save Changes**.