### The caption editor

![markdown_00_tools.PNG](https://bitbucket.org/repo/zpBnoa/images/3535505144-markdown_00_tools.PNG)

For editing the content of your captions, you should use the available textarea to write your content. Above, you'll find a toolbar filled with buttons to help you format your text.
Our captions use markdown syntax, but we don't expect you to learn that syntax by heart, so these buttons will do the heavy lifting for you.

____
### Text size

![markdown_01_headings.png](https://bitbucket.org/repo/zpBnoa/images/3919403849-markdown_01_headings.png)

Use one of the 6 heading buttons to change the size of your text. H1 is the biggest, H6 is the smallest. This option will apply to an entire line.


____
### Bold and Italic

![markdown_02_emphazis.png](https://bitbucket.org/repo/zpBnoa/images/2227879460-markdown_02_emphazis.png)

Start by selecting the text you would like to bolden or italicize. Press the corresponding button to apply the style. This option will affect the text you have selected.

____
### Alignment

![markdown_03_alignment.png](https://bitbucket.org/repo/zpBnoa/images/3789703230-markdown_03_alignment.png)

Use one of the alignment buttons to choose where your text should align itself. the `@center` is the markdown syntax that will align your text to the middle of the caption. The whole line of text will be affected by the change.

____
### Color

![markdown_04_color.png](https://bitbucket.org/repo/zpBnoa/images/1217651851-markdown_04_color.png)

Click the color button to select a color. If you have a BindTuning theme applied to the page, the pallete picker will appear by default.
These colors are provided by the theme and will vary with your branding. So in another page using a different theme but showing the same content, the colors will adapt to that page.
If you're not seeing the color pallete, make sure you have a BindTuning theme applied and that your theme is updated to the latest version

![markdown_05_swatches.PNG](https://bitbucket.org/repo/zpBnoa/images/925489518-markdown_05_swatches.PNG)

If you want to pick a custom color, you can open the color picker by pressing the <i class="fa fa-crosshairs"></i> icon. Colors selected through here are static and will always show the same regardless of the page.

![markdown_06_picker.PNG](https://bitbucket.org/repo/zpBnoa/images/2411650666-markdown_06_picker.PNG)

____
### Links

![markdown_07_links.png](https://bitbucket.org/repo/zpBnoa/images/1620255421-markdown_07_links.png)

Clicking the links button will open a small form to help you add links to your caption. Links can have a background and text color. They can also look like simple text links or colored buttons.

____
### Icons

![markdown_08_icons.png](https://bitbucket.org/repo/zpBnoa/images/1949736306-markdown_08_icons.png)

Click the smiley icon to open the icon picker. The icons are split into tabs depending on their category. Clicking an icon will open the size picker so you can select just how big you want the icon to be.

![markdown_09_icons_list.PNG](https://bitbucket.org/repo/zpBnoa/images/2540817124-markdown_09_icons_list.PNG)

### Preview

![markdown_11_preview.png](https://bitbucket.org/repo/zpBnoa/images/2688602889-markdown_11_preview.png)

You can use the preview button to toggle between editing and preview mode. In preview mode, you can't edit your text and get to see what the text will look like after publishing.