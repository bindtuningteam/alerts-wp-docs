1. Click the **BindTuning Tab** on the Ribbon and click on **Edit Web Parts**.  The property pane will appear to the left of the web part;

    ![bindtuning-tab](../images/classic/01.general-options.png)   

2. Click the cogwheel icon. This will open the properties form;

    ![property-pane](../images/classic/02.property-pane.png)

3. **Configure** the web part according to the settings described in the **[Web Part Properties](./general.md)**;
   
4. When you're done, click the **Save** button and the web part will reload.

    ![form-save](../images/classic/04.form-save.png)

    Note that the Appearence options won't be visible until you reload the page.