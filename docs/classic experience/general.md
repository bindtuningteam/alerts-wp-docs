On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [List Settings](./listsettings.md)
- [Advanced Option](./advanced.md)
- [Performance Option](./performance.md)
- [Appearance Option](./appearance.md)
- [Message Option](./message.md)

![Settings](../images/classic/settings.png)

The global settings form let you apply options to **all the web parts** on the page at once. To use the form, follow the steps:

- [Configure Global Settings](./globalsettings.md)

![global_01_tab.PNG](../images/classic/05.globalsettings.png)