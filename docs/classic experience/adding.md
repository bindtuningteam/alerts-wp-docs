1. Open the page where you want to add the web part and click on the **Edit** button;

    ![edit-page](../images/classic/01.edit-page.png)

2. After the refresh, select the **Insert Tab** on the Ribbon and then click on **Web Part**;

    ![insert-tab](../images/classic/02.insert-tab.png)

3. On the categories list, select **BindTuning Add-in** and then click **BindTuning Alerts**;

    ![select-part](../images/classic/03.select-part.png)

4. With the web part selected, click **Add**;

5. Click the **Save** button before continuing.

    ![save-page](../images/classic/05.save-page.png)


By saving the page, you safeguard your progress and make sure it won't be lost to closing or reloading the page accidentaly.