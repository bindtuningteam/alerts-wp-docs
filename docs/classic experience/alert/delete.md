1. Open the site where you have the Alerts web part installed;

2. Click **Bindtuning**, and then **Edit Web parts** to edit the web part;

3. On the web part sidebar click the **Manage Alerts** icon;

4. The list of alerts will appear. Click the **trash can** icon to delete the alert that you want to remove;

5. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the alert will be removed.

![deleteanalert.gif](https://bitbucket.org/repo/ekxgn66/images/3424848684-deleteanalert.gif)