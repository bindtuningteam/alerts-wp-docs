1. Open the site where you have the Alerts web part installed;

2. Click **Bindtuning**, and then **Edit Web parts** to edit the web part;

3. On the web part sidebar bar, click the ➕ button;

	![addnewalert.gif](https://bitbucket.org/repo/ekxgn66/images/1080837074-addnewalert.gif)


4. Fill out the form that pops up. You can check out what you need to do in each setting in the <a href="../../global/alertsettings">Alerts Settings</a> section of this User Guide;

	![newalertmodal.png](https://bitbucket.org/repo/ekxgn66/images/2156852497-newalertmodal.png)


5. After setting everything up, click on the **Preview** button if you want to see how everything looks on the page, or click on **Save** or **Save and create another**.

	![buttons_newalert.png](https://bitbucket.org/repo/ekxgn66/images/2357822962-buttons_newalert.png)

<p class="alert alert-success">Clicking <b>Save and create another</b> will keep the form open, so you can add more alerts to your page without closing the form.</p>