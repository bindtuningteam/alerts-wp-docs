1. Open the site where you have the Alerts web part installed;

2. Click **Bindtuning**, and then **Edit Web parts** to edit the web part;

	![editanalert.gif](https://bitbucket.org/repo/ekxgn66/images/2816642940-editanalert.gif)

3. On the web part sidebar click the **Manage Alerts** icon;

4. The list of alerts will appear. Click the **Pencil icon** to edit an alert;

5. Change the <a href="../../global/alertsettings">Alerts Settings</a> as necessary to re-configure the alert.

	![editalertmodal.png](https://bitbucket.org/repo/ekxgn66/images/1694895613-editalertmodal.png)

6. Done editing? Click on the **Preview** button if you want to see how everything looks on the page or on **Save Changes**.