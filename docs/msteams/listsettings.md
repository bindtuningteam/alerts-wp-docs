### Create a list

This list is what you will use to store your alerts. Let's create your list:

1. On the Tab click on the dropdown and then settings. If is the first time adding the **Alert**, you can skip this process. 
2. On The panel select the Create New BT Alerts List
3. Type the name of the list and hit **Create the List**;

    ![list url](../images/modern/04.createlist.png)     
    
4. The list will be created on your current Team Site and connected with the Web Part;

____
### Add List URL

If you have already configured a BindTuning alerts list that you intend to display, you need to paste the **list URL** that you have copied when you finished to create the list. This list will be stored on a SharePoint site.

![list url](../images/modern/03.listurl.png)

<p class="alert alert-info">We recommend you use relative paths in this field. So instead of using a URL like  https://company.sharepoint.com/sites/Home/Lists/Alerts you should use something like <b>/sites/Home/Lists/Alerts</b>. This will ensure that the web part will work regardless of how you’re accessing the site. </p>