1. Open the Team on **Teams panel** that you intend to add the Web Part; 
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)

3. Click on the **Manage Alerts** icon;

	![Edit](../../images/modern/14.edit_alert.png)

4. Change the <a href="../../global/alertsettings">Alerts Settings</a> as necessary to re-configure the alert.

	![editalertmodal.png](https://bitbucket.org/repo/ekxgn66/images/1694895613-editalertmodal.png)

5. Done editing? Click on the **Preview** button if you want to see how everything looks on the page or on **Save Changes**.