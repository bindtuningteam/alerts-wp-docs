1. Open the Team on **Teams panel** that you intend to remove the Web Part; 
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)

3. Click on the **Manage Alerts** icon;

	![Manage](../../images/msteams/manage_alert.png)

4. The list of alerts will appear. Click the **trash can** icon to delete the alert that you want to remove;

	![Delete](../../images/modern/13.delete_alert.png)

5. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the alert will be removed.	